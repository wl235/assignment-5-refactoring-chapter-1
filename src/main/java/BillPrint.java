import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public Play playFor(Performance perf){
		Play play = plays.get(perf.getPlayID());
		if (play == null) {
			throw new IllegalArgumentException("No play found");
		}
		return play;
	}

	public int amountFor(Performance perf){
		int result = 0;
		switch (playFor(perf).getType()){
			case "tragedy": result = 40000;
				if (perf.getAudience() > 30){
					result += 1000 * (perf.getAudience() - 30);
				}
				break;
			case "comedy":  result = 30000;
				if (perf.getAudience() > 20) {
					result += 10000 + 500 * (perf.getAudience() - 20);
				}
				result += 300 * perf.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  playFor(perf).getType());
		}
		return result;
	}

	public int volumeCreditsFor(Performance perf){
		int result = 0;
		result += Math.max(perf.getAudience() - 30, 0);

		if ( playFor(perf).getType().equals("comedy")) {
			result += Math.floor((double) perf.getAudience() / 5.0);
		}
		return result;
	}

	public int totalVolumeCredits(){
		int result = 0;
		for (Performance perf: performances){
			result += volumeCreditsFor(perf);
		}
		return result;
	}


	public String usd(int nCents){
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return "$" + numberFormat.format(nCents / 100.00);
	}

	public int totalAmount(){
		int result = 0;
		for (Performance perf: performances){
			result += amountFor(perf);
		}
		return result;
	}
	public String statement() {

		String result = "Statement for " + this.customer + "\n";
		for (Performance perf: performances) {

			// print line for this order
			result += "  " + playFor(perf).getName() + ": " + usd(amountFor(perf)) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}


		result += "Amount owed is " + usd(totalAmount()) + "\n";
		result += "You earned " +  totalVolumeCredits() + " credits" + "\n";
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
